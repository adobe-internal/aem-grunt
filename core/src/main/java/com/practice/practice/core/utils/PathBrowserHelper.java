package com.practice.practice.core.utils;

import org.apache.commons.lang3.StringUtils;

import com.adobe.cq.sightly.WCMUsePojo;

public class PathBrowserHelper extends WCMUsePojo{
	
	private boolean newTab;
	
	private String link;

	@Override
	public void activate() throws Exception {
		link = addHtmlIfContentPath(get("url", String.class));
		newTab = checkDomainName(get("url", String.class));
	}
	
	public static String addHtmlIfContentPath(String path){
		if(StringUtils.isNotEmpty(path)){
			if(path.startsWith("/content") && !path.substring(path.lastIndexOf("/")).contains(".")){
				return path.concat(".html");
			}		
		}else{
			return StringUtils.EMPTY;
		}
		return path;
	}
	
	public static boolean checkDomainName(String path){
		if(StringUtils.isNotEmpty(path)){
			if(!path.startsWith("/content") && !path.contains("ti.com") && !path.contains("tij.co.jp")){
				return true;
			}
		}
		return false;
	}

	public String getLink() {
		return link;
	}

	public boolean isNewTab() {
		return newTab;
	}
}