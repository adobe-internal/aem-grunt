package com.practice.practice.core.models;

import java.util.List;

import com.adobe.cq.sightly.WCMUsePojo;
import com.practice.practice.core.bean.TextModelBean;
import com.practice.practice.core.utils.PropertiesUtils;

public class FeatureToolModel extends WCMUsePojo {

	List<TextModelBean> listItemsProp;
	
	String[] descListItems;
		
	@Override
	public void activate() throws Exception {
		descListItems = getProperties().get("listItems", String[].class);
		listItemsProp = PropertiesUtils.getListFromStringArray(descListItems,
				TextModelBean.class);
	}

	public List<TextModelBean> getListItemsProp() {
		return listItemsProp;
	}

}