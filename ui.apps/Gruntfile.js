module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		clean: ['src/**/main.css', 'src/**/main.js'],
		sass: {                              // Task 
			dist: {                            // Target 
				options: {                       // Target options 
					style: 'expanded'
				},
				files: {                         // Dictionary of files 
					'src/main/content/jcr_root/etc/designs/practice/clientlib-site/css/main.css': 'src/**/main.scss'       // 'destination': 'source'
				}
			}
		},
		cssmin: {
			  target: {
			    files: [{
			      expand: true,
			      cwd: 'src/main/content/jcr_root/etc/designs/practice/clientlib-site/css',
			      src: ['*.css', '!*.min.css'],
			      dest: 'src/main/content/jcr_root/etc/designs/practice/clientlib-site/css',
			      ext: '.min.css'
			    }]
			  }
			},
    jshint: {
        all: ['Gruntfile.js', 'src/**/*.js']
      }
    });

    // Load required modules
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
 
    // Task definitions
    grunt.registerTask('default', ['clean', 'sass', 'cssmin', 'jshint']);
};