(function($, $document) {
	
	jQuery.validator.register({
		  selector: "[data-myapp-maxlength]",
		  validate: function(el) {
		    var maxlength = parseInt(el.attr("data-myapp-maxlength"), 10);

		    if (isNaN(maxlength)) {
		      return;
		    }

		    var length = el.val().length;

		    if (length > maxlength) {
		      return "The field needs to be maximum " + maxlength + " characters. It currently has a length of " + length + ".";
		    }
		  }
		});

	$(document).on("foundation-contentloaded", function(e) {
		/* updateErrorUI is used to called clean method of validator */
		$(" [data-myapp-maxlength]").updateErrorUI();
	});
}(jQuery));

